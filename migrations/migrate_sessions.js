var config = require('../config/config');
var crypto = require('crypto');
var MongoClient = require('mongodb').MongoClient;
MongoClient.connect(config.mongo.dburi, {db:{}, server:{auto_reconnect:true, poolSize:1}}, function (err, db) {
    if (err) {
        console.log('error connecting to mongo');
        throw err;
    }
    //set indexes
    db.collection('sessions', function (err, collection) {
        if (!err) {
            collection.find({'session':{$regex:'profile'}}, {session:true}).toArray(function (err, items) {
                if (err) {
                    console.log('failed to get sessions');
                    throw err;
                } else {
                    var profiles = items.map(function (item) {
                        var parsed = JSON.parse(item.session);
                        if (parsed.profile && parsed.profile.email) {
                            //Set id
                            var profile = parsed.profile;
                            profile._id = 'guest' + crypto.createHash('md5').update(profile.email.toLowerCase().trim()).digest("hex");
                            profile.type = 'guest';
                            profile.name = profile.first_name + ' ' + profile.last_name;
                            profile.photo = profile.photo_big || profile.photo;
                            return profile;
                        }
                        return false;
                    });
                    if (profiles && profiles.length) {
                        //Save to user
                        console.log('got ' + profiles.length);
                        db.collection('profiles', function (err, profilecollection) {
                            if (err) {
                                console.log('failed to get profiles');
                                throw err;
                            } else {
                                profiles.forEach(function (profile) {
                                    profilecollection.save(profile, {upsert:true, safe:false});
                                });
                            }
                        });
                    }
                }
            });
        }
        else {
            console.log('failed to get collection');
            throw err;
        }
    });
});
