var config = require('../config/config');
var MongoClient = require('mongodb').MongoClient;
var async = require('async');
MongoClient.connect(config.mongo.dburi, {db: {}, server: {auto_reconnect: true, poolSize: 1}}, function (err, db) {
    if (err) {
        console.log('error connecting to mongo');
        throw err;
    }
    //Do async indexes
    //Do parallel async
    console.log('begin converting');
    async.parallel(
        [
            function (callback) {
                //Set registration time index
                db.collection('registrations-times', function (err, collection) {
                    if (!err) {
                        console.log('updating registration ttl index');
                        collection.find({date: {$exists: true, $type:1}}, {_id: 1, date: 1}, {snapshot: true}).each(function (err, data) {
                            if (err) {
                                console.log(err);
                                callback(err);
                            } else if (data!=null && typeof data.date==='number') {
                                console.log("updating registrations-times "+data._id);
                                collection.update({_id: data._id}, {$set: {date: new Date(data.date)}}, {safe: false, upsert: false});
                            }
                        });

                    }
                    callback(err);
                });
            },
            function (callback) {
                //Set registration time index
                db.collection('users-online', function (err, collection) {
                    if (!err) {
                        console.log('updating users-online ttl index');
                        collection.find({lastseen: {$exists: true, $type:1}}, {_id: 1, lastseen: 1}, {snapshot: true}).each(function (err, data) {
                            if (err) {
                                console.log(err);
                                callback(err);
                            } else if (data!=null && typeof data.lastseen==='number') {
                                console.log("updating users-online "+data._id);
                                collection.update({_id: data._id}, {$set: {lastseen: new Date(data.lastseen)}}, {safe: false, upsert: false});
                            }
                        });

                    }
                    callback(err);
                });
            },
            function (callback) {
                //Set registration time index
                db.collection('chat-tokens', function (err, collection) {
                    if (!err) {
                        console.log('updating chat-tokens ttl index');
                        collection.find({issued: {$exists: true, $type:1}}, {_id: 1, issued: 1}, {snapshot: true}).each(function (err, data) {
                            if (err) {
                                console.log(err);
                                callback(err);
                            } else if (data!=null && typeof data.issued==='number') {
                                console.log("updating chat-tokens "+data._id);
                                collection.update({_id: data._id}, {$set: {issued: new Date(data.issued)}}, {safe: false, upsert: false});
                            }
                        });

                    }
                    callback(err);
                });
            },
            function (callback) {
                //Set registration time index
                db.collection('messages', function (err, collection) {
                    if (!err) {
                        console.log('updating messages ttl index');
                        collection.find({received: {$exists: true, $type:1}}, {snapshot: true}).each(function (err, data) {
                            if (err) {
                                console.log(err);
                                callback(err);
                            } else if (data!=null && typeof data.received==='number') {
                                console.log("updating messages "+data._id);
                                collection.update({_id: data._id}, {$set: {received: new Date(data.received)}}, {safe: false, upsert: false});
                            }
                        });

                    }
                    callback(err);
                });
            }
        ], function (err, results) {
            console.log('end converting');
            if (err) {
                console.log("err:" + err);
                process.exit(-1);
            } else {
                console.log('success');
            }

        });

});