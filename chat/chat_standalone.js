var sockjs = require('sockjs');
var ObjectID = require('mongodb').ObjectID;
var EventEmitter = require('events').EventEmitter;
var async = require('async');
var dataHelper = require('../db/data_helpers');
var uid = require('node-uuid');
var _ = require('underscore');
var moment = require('moment');

exports.createChatServer = function (mongoConnection, httpServer) {
    moment.lang('ru');
    var urlParse = /^\/chat_poll\/([0-9-.a-z_]*).+/i;
    var db = mongoConnection;
    var channelsOnline = {};

    var sockjs_opts = {
        sockjs_url:"http://cdn.sockjs.org/sockjs-0.3.min.js",
        websocket:false, //We don't use socket for now because heroku doesn't support it for now
        log:function (severity, message) {
            console.log(severity + ":" + message);
        }
    };
    var server = sockjs.createServer(sockjs_opts);

    function ChatEmitter(name) {
        this.name = name;
        EventEmitter.call(this);
        ChatEmitter.prototype.__proto__ = EventEmitter.prototype;
    }

    var emitter = new ChatEmitter('chat-emitter');
    var message_emitter = new ChatEmitter('chat-message-emitter');
    emitter.setMaxListeners(0);
    message_emitter.setMaxListeners(0);

    function onMessage(msg) {
        msg.received = new Date();
        msg.read = false;
        msg._id = uid.v4();
        db.collection('messages', function (err, collection) {
            if (!err) {
                //Insert message
                collection.save(msg, {upsert:true, safe:false});
            }
        });
        message_emitter.emit(msg.to, msg);
    }


    function onPubMessage(channel, message) {
        //Global messages
        emitter.emit(channel, JSON.parse(message));
    }

//Setup listeners
    server.on('connection', function (conn) {
        if (!conn) {
            console.log(" [.] open event received. con is null. closing");
            return;
        }
        //Add send method for objects
        //TODO: Move this to Connection prototype
        conn.send = function (obj) {
            conn.write(JSON.stringify(obj))
        }
        conn.fatalError = function (message) {
            conn.send({type:'error', message:message});
            conn.close(400, message);
        }
        conn.on('data', function (message) {
            onData(conn, message);
        });

        //Bug fix for heroku not closing connections
        onOpen(conn);
        console.log(" [.] open event received");
        var t = setInterval(function () {
            try {
                conn._session.recv.didClose();
            } catch (x) {
            }
        }, 15000);
        conn.on('close', function () {
            console.log(" [.] close event received");
            clearInterval(t);
            onClose(conn);
        });
    });

    function onOpen(connection) {
        var parsed = urlParse.exec(connection.url);
        if (parsed.length > 1) {
            connection.token = parsed[1];
        } else {
            connection.fatalError('bad token');
        }
        //Query mongo. Check if provided token is correct
        db.collection('chat-tokens', function (err, collection) {
            if (err) {
                connection.fatalError('error opening db');
            } else {
                try {
                    collection.findOne({_id:new ObjectID(connection.token)}, function (err, item) {
                        // determine value
                        if (err || !item) {
                            connection.fatalError('token not found');
                        } else {
                            //Authorized
                            if (item.user && item.user.id) {

                                dataHelper.getUserProfile(db, item.user.id, function (err, item) {
                                    if (!err && item) {
                                        //Now connection ready
                                        connection.userid = item._id.toString();
                                        connection.user = item;
                                        onAuthorized(connection);
                                        connection.authorized = true;
                                    } else {
                                        connection.fatalError('token not belongs to any user');
                                    }
                                });

                            } else {
                                connection.fatalError('token not belongs to any user');
                            }
                        }
                    });
                }
                catch (err) {
                    connection.fatalError('bad token');
                }
            }
        });
    }


    function updateUserOnline(connection) {
        if (connection.userid && connection.user && connection.user.type === 'builtin') {
            //Update last_visit
            db.collection('profiles', function (err, collection) {
                if (!err) {
                    try {
                        collection.update({_id:new ObjectID(connection.userid)}, {$set:{last_seen:Date.now()}}, {safe:false});
                    } catch (err) {

                    }
                }
            });
        }
    }

    function onData(connection, message) {
        if (connection.authorized) {
            try {
                var messageObject = JSON.parse(message);
                if (messageObject.type) {
                    //Handle command
                    if (commandHandler.hasOwnProperty(messageObject.type)) {
                        commandHandler[messageObject.type](connection, messageObject);
                    }
                } else {
                    //It's message
                    onConnectionMessage(connection, messageObject);
                }
            } catch (err) {
                connection.fatalError(err.toString());
            }
        } else {
            connection.fatalError('unathorized');
        }
    }

    function onConnectionMessage(connection, message) {
        //Publish to hub
        //Make an message channel id. It's parsed in hub.sub.on('message') above
        if (message && message.to && message.body && typeof message.body == 'string' && message.body.length < 1000 && message.from === connection.userid && connection.user) {
            if (connection.nextMessageTime > (Date.now()+(5000-(message.body.length / 100))) ) {
                connection.send({type:'error', message:'Вы слишком часто посылаете длинные сообщения. Повотрите '+moment(connection.nextMessageTime).fromNow()});
            } else {
                connection.nextMessageTime = Date.now() + (message.body.length / 200) * 5000;//200 symbols per 10 sec
                onMessage({to:message.to, from:connection.userid, fromProfile:connection.user, body:message.body, related:message.related});
            }
        } else {
            connection.send({type:'error', message:'Сообщение неправильно составлено или длиннее 1000 символов'});
        }
    }

//Callbacks
    function getOnlineCallback(conn) {
        return function (user) {
            conn.send({type:'online', user:user});
        };
    }

    function getOfflineCallback(conn) {
        return function (user) {
            conn.send({type:'offline', user:user});
        };
    }

    function getMessageCallback(conn) {
        return function (message) {
            if (message.hasOwnProperty('type')) {
                conn.send(message);
            } else {
                conn.send({type:'message', message:message});
            }
        };
    }

    function getMessageChannelCallback(conn, channel) {
        return function (message) {
            if (message.hasOwnProperty('type')) {
                message.chanell = channel;
                conn.send(message);
            } else {
                conn.send({type:'message', channel:channel, message:message});
            }
        };
    }

    function onAuthorized(connection) {
        //Setup callbacks
        connection.onlineCallback = getOnlineCallback(connection);
        connection.offlineCallback = getOfflineCallback(connection);
        connection.messageCallback = getMessageCallback(connection);

        emitter.on('online', connection.onlineCallback);
        emitter.on('offline', connection.offlineCallback);
        message_emitter.on(connection.userid, connection.messageCallback);
        //Send authorization
        connection.send({type:'authorized', profile:connection.user});
    }

    function onClose(connection) {
        if (connection.authorized && connection.user) {
            emitter.removeListener('online', connection.onlineCallback);
            emitter.removeListener('offline', connection.offlineCallback);
            message_emitter.removeListener(connection.userid, connection.messageCallback);
            if (connection.channels) {
                for (var channel in connection.channels) {
                    if (connection.channels.hasOwnProperty(channel)) {
                        channelsOnline[channel] = Math.max(0, (channelsOnline[channel] || 0) - 1);
                        message_emitter.emit(channel, {type:'leave', profile:connection.user, online:channelsOnline[channel]});
                        message_emitter.removeListener(channel, connection.channels[channel]);
                    }
                }
            }
        }
    }

    var commandHandler = {};
    commandHandler.read = function (connection, message) {
        if (message.ids && _.isArray(message.ids)) {
            db.collection('messages', function (err, collection) {
                if (!err) {
                    collection.update({_id:{$in:message.ids}}, {$set:{read:true}}, {safe:false, multi:true});
                }
            });
        }
    }

    commandHandler.subscribe = function (connection, message) {
        if (message.channel) {
            if (!connection.channels) {
                connection.channels = {};
            }
            //Push new channel
            if (!connection.channels.hasOwnProperty(message.channel)) {
                connection.channels[message.channel] = getMessageChannelCallback(connection, message.channel);
                channelsOnline[message.channel] = (channelsOnline[message.channel] || 0) + 1;
                message_emitter.emit(message.channel, {type:'join', profile:connection.user, online:channelsOnline[message.channel]});

                message_emitter.on(message.channel, connection.channels[message.channel]);

            }
        }
    }

    commandHandler.unsubscribe = function (connection, message) {
        if (message.channel) {
            if (connection.channels && connection.channels.hasOwnProperty(message.channel)) {
                message_emitter.removeListener(message.channel, connection.channels[message.channel]);
                channelsOnline[message.channel] = Math.max(0, (channelsOnline[message.channel] || 0) - 1);
                message_emitter.emit(message.channel, {type:'leave', profile:connection.user, online:channelsOnline[message.channel]});
                delete connection.channels[message.channel];
            }
        }
    }

    server.installHandlers(httpServer, {prefix:'/chat_poll/[0-9-.a-z_]*'});
}