var config = {};

config.mongo = {};
config.web = {};
config.mail = {};

config.web.port = process.env.PORT || 3000;
config.mongo.dburi = process.env.MONGO_URI || 
	process.env.MONGOHQ_URL || 
	process.env.MONGOLAB_URI || 
	"mongodb://127.0.0.1:27017/date-programmer";
config.mail.domain = "mail.loveplusplus.ru";
config.mail.from = "Love++ mailer <sender@loveplusplus.ru>";
config.mail.from_name = "Love++ mailer";
config.mail.smtp = {
	pool: true,
	host: process.env.MAILGUN_SMTP_SERVER || 'smtp.mailgun.org',
	port: process.env.MAILGUN_SMTP_PORT || '587',
	secure: false, // use SSL
	auth: {
		user: 'postmaster@mail.loveplusplus.ru',
		pass: 'be217a5acf0b2ada424490abbcfcd024'
	}
};
config.isProduction = process.env.NODE_ENV === 'production';

config.redis = config.redis || {};
config.redis.url = process.env.REDISCLOUD_URL || 'redis://rediscloud:rkLXFDHBv9knnWbG@pub-redis-11492.us-east-1-4.3.ec2.garantiadata.com:11492';


module.exports = config;
