$(function () {
    $(document).on('chat-ready', function (evt, data) {
        var cid = data && data.user && data.user.id;
        if (window.webkitNotifications && window.webkitNotifications.checkPermission() !== 0) {
            $('#browserNotify').show().find('button').click(function () {
                window.webkitNotifications.requestPermission();
                $('#browserNotify').hide();
            })
        }
        $(document).on('chat-message', function (evt, data) {
            var msg = data.message;
            if (msg && window.currentDialog !== msg.from && cid === msg.to) {
                //Notify
                notify(msg);
            }
        });

        var $connClosed = null;

        $(document).on('chat-closed', function (evt, data) {
            if (!$connClosed) {
                $connClosed = $.pnotify({
                    title:'Соединение разорвано',
                    text:"Соединение с сервером чата разорвано. Восстанавливаем",
                    type:'error',
                    nonblock: true,
                    hide: false,
                    closer: false,
                    sticker: false
                });
            }
        });

        $(document).on('chat-reconnected', function (evt, data) {
            if ($connClosed && $connClosed.pnotify_remove) {
                $connClosed.pnotify_remove();
                $connClosed = null;
            }
            $.pnotify({
                title:'Соединение восстановлено',
                text:"Соединение с сервером чата восстановлено.",
                type:'success',
                sticker:false
            });
        });


        function notify(msg) {
            $.pnotify({
                title:'Новое сообщение',
                text:views['message_notification']({user:msg.fromProfile, body:msg.body}),
                type:'info',
                sticker:false,
                icon:'icon-envelope',
                styling:'bootstrap'
            });
            //Check browser notification support
            if (window.webkitNotifications && msg.fromProfile && msg.body) {
                var gravatar = 'http://www.gravatar.com/avatar/' + msg.fromProfile.hash + '?s=100&d=retro';
                var imgUrl = msg.fromProfile.photo ? msg.fromProfile.photo : gravatar;
                var notification = window.webkitNotifications.createNotification(
                    imgUrl, 'Новое сообщение от: ' + msg.fromProfile.name, msg.body);
                notification.ondisplay = function (event) {
                    setTimeout(function () {
                        event.currentTarget.cancel();
                    }, 15000);
                }
                notification.show();
            }
        }
    });
});