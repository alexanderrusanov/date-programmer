function initDialogs() {
    $(document).on('chat-ready', function () {
        var $dialog = null;
        var readed = [];
        moment.lang('ru');
        var contacts = {};
        //Set moment updater
        setInterval(function () {
            $('.timestamp').each(function (index, item) {
                var $item = $(item);
                $item.text(moment(parseInt($item.data('time'))).fromNow());
            })
        }, 10000);

        setInterval(function () {
            if (readed.length > 0) {
                //Push to server
                chat.send({type: 'read', ids: readed});
                readed = [];
            }
        }, 5000);

        $('#dialogs .dialog-contact').each(function (index, item) {
            contacts[$(item).data('contact-id')] = $(item);
        });
        $('#dialogs').on('click', '.open-dialog', function (e) {
            //Show dialog with user
            if ($dialog) {
                $dialog.remove();
            }
            var $this = $(this);

            //Query
            $.ajax({
                url: '/chat/history/' + $this.data('contact-id'),
                cache: false,
                dataType: 'json'
            })
                .done(function (result) {
                    $dialog = $(views['dialog'](result));
                    $dialog.cid = result.cid;
                    $dialog.toId = $this.data('contact-id');
                    window.currentDialog = $dialog.toId;
                    $this.parents('.profile-block').after($dialog);
                    //Scroll to bottom
                    var msgList = $dialog.find('.msg-list');
                    $dialog.find('.msg-holder').scrollTop(msgList.height());
                    var $form = $dialog.find('form');
                    var $sendonenter = $('#sendonenter');
                    $form.find('textarea').keypress(function (e) {
                        if ($sendonenter.hasClass('active')) {
                            if (event.which == 13 && !event.shiftKey) {
                                $form.submit();
                            }
                        } else {
                            if ((e.keyCode === 10 || e.keyCode == 13) && (e.ctrlKey || e.metaKey)) {
                                $form.submit();//!Chrome return key code 10
                            }
                        }
                    });
                    $form.submit(function (e) {
                        var text = $(this).find('textarea').val();
                        var msg = {from: result.cid, to: $dialog.toId, body: text, read: false, received: new Date().getTime()};
                        if (chat.send(msg)) {
                            //Append last
                            msgList.append(views['dialog_message']({msg: msg, cid: result.cid}));
                            $(this).find('textarea').val('');
                            $dialog.find('.msg-holder').scrollTop(msgList.height());
                        }
                        e.preventDefault();
                        return false;
                    });
                });
            e.preventDefault();
            return false;
        });

        $(document).on('chat-message', function (evt, data) {
            var msg = data.message;
            if ($dialog && $dialog.toId === msg.from) {
                //Append
                $dialog.find('.msg-list').append(views['dialog_message']({msg: msg, cid: $dialog.cid}));
                $dialog.find('.msg-holder').scrollTop($dialog.find('.msg-list').height());
                readed.push(msg._id);
            } else if (contacts.hasOwnProperty(msg.from)) {
                var counter = contacts[msg.from].find('.unread-counter');
                counter.text(parseInt(counter.text()) + 1);
                counter.parent().removeClass('muted').addClass('unread');
            } else {
                var contact = $(views['dialog_contact']({user: msg.fromProfile, unreadCount: 1}));
                $('#dialogs').prepend(contact);
                contacts[msg.from] = contact;
                $('#nodialogs').hide();
            }
        });
        $(document).on('chat-online', function (evt, message) {

        });
        $(document).on('chat-offline', function (evt, message) {

        });
    })
}