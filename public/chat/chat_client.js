var chat = chat || {};
var $inlineChat = null;
chat.start = function (initData) {
    if (!initData || !initData.token) {
        return;
    }

    function initUi(initData) {
        //Add inline message
        //Subscribe to inline sending
        //Render
        if (!$inlineChat) {
            //Create chat
            $inlineChat = $(views['inline_message']());
        }
        //Enable chat
        if ($inlineChat) {
            var $form = $inlineChat.find('form#inline-message');
            $form.find('.send-message-block textarea, .send-message-block .btn').removeAttr('disabled');
            $form.find('textarea').keypress(function (e) {
                if ((e.keyCode === 10 || e.keyCode == 13) && e.ctrlKey) {
                    $form.submit();//!Chrome return key code 10
                }
            });

            $form.submit(function (e) {
                var text = $(this).find('textarea').val();
                var sendResult = chat.send({from:initData.user.id, to:$inlineChat.id, body:text});
                var alert;
                if (sendResult) {
                    alert = $('<div class="alert message-status text-center alert-success"><i class="icon-envelope"></i>&nbsp;Сообщение отправлено!</div>');

                } else {
                    alert = $('<div class="alert message-status text-center alert-error"><i class="icon-exclamation-sign"></i>&nbsp;Сообщение не отправлено</div>');
                }
                setTimeout(function () {
                    alert.remove();
                }, 3000);
                $inlineChat.prev().after(alert);
                $inlineChat.detach();
                e.preventDefault();
                return false;
            });
        }
        $(document).on('click', '.profile-block .send-message', function (e) {
            var $this = $(this);
            $inlineChat.id = $this.data('contact-id');
            $inlineChat.find('form#inline-message textarea').val('');
            var $profile = $this.parents('.profile-block').after($inlineChat);
            e.preventDefault();
            return false;
        });
        //Set msg indicator
        $('#msg .msgcount').html('(' + ( initData.unread || 0) + ')');
    }

    $(function () {
        if (initData.token) {
            initUi(initData);
            //Connect after 500 msec to not load server much
            setTimeout(function () {
                connect(initData.token)
            }, 100);
        } else {
            //TODO: No csrf token! Go away!
        }
    });

    var retries = 0;
    var reconnecting = false;

    function connect(token) {
        //Setup connection
        var sock = new SockJS('/chat_poll/' + token);
        chat.sock = sock;
        var sendMessage = function (obj) {
            if (typeof obj === 'string') {
                return sock.send(obj);
            } else {
                return sock.send($.toJSON(obj));
            }
        }
        chat.send = sendMessage;

        sock.onopen = function () {
            retries = 0;
            $(document).trigger('chat-connected', initData);
        };
        sock.onmessage = function (e) {
            var message = $.parseJSON(e.data);
            onMessage(message, sock);
        };
        sock.onclose = function () {
            $(document).trigger('chat-closed');
            retries++;
            reconnecting = true;
            setTimeout(function () {
                connect(token);
            }, 1000 * retries);
        };
    }


    function onMessage(message, socket) {
        if (handlers.hasOwnProperty(message.type)) {
            handlers[message.type](message, socket);
        } else {
            $(document).trigger('chat-' + message.type, message);
        }

    }

    var handlers = {};
    handlers.authorized = function (message, socket) {
        if (!reconnecting) {
            $(document).trigger('chat-ready', initData);
        } else {
            $(document).trigger('chat-reconnected', initData);
        }
    }
    handlers.message = function (message, socket) {
        $(document).trigger('chat-message', message);
    }
    handlers.online = function (message, socket) {
        $(document).trigger('chat-online', message);
    }
    handlers.offline = function (message, socket) {
        $(document).trigger('chat-offline', message);
    }
}

