$(function () {
    $('#sex .btn').click(function () {
        $('#sex-input').val($(this).data('sex')).trigger('change');
    });
    $('#near-me').click(function (e) {
        if (geo_position_js.init()) {
            geo_position_js.getCurrentPosition(onGeoPos, noGeo);
        }
        else {
            noGeo();
        }
        e.preventDefault();
        return false;
    });

    function onGeoPos(position) {
        $('#lat').val(position.coords.latitude.toFixed(2));
        $('#lng').val(position.coords.longitude.toFixed(2));
        //$('#near-me').unbind('click');
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
                            'latLng': new google.maps.LatLng(position.coords.latitude.toFixed(2),
                            position.coords.longitude.toFixed(2)), region:'ru'
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK && results && results.length > 0 && results[0].address_components) {
                var loc='';
                for (var ac=0;ac<results[0].address_components.length;ac++){
                    var types = results[0].address_components[ac].types;
                    if (types && types.length){
                        for (var t=0;t<types.length;t++){
                            if (types[t]==='locality' || types[t]==='country'){
                                loc+=results[0].address_components[ac].long_name+", ";
                                break;
                            }
                        }
                    }
                }
                loc=loc.replace(/[\s,]+$/,'');
                $('#location').val(loc).trigger('change');
            }
        });
    }

    function noGeo() {
        $('#near-me').unbind('click').addClass('disabled');
    }

    var input = document.getElementById('location');
    var options = {
        types: ['(cities)']
    };
    var placeChanged = false;
    $('#location').keypress(function(e) {
        if (e.which == 13 && !placeChanged) {
            return false;
        }
        placeChanged = false;
    });
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        $('#lat').val(place.geometry.location.lat().toFixed(2));
        $('#lng').val(place.geometry.location.lng().toFixed(2)).trigger('change');
        placeChanged = true;
    });
});