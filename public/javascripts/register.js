$(function () {
    $('#run').click(function (evt) {
        //Get the text of script
        try {
            var scriptText = $('#console').val();
            var result = CoffeeScript.compile(scriptText);
            eval(result);
        }
        catch (e) {
            printError(e.message);
        }
        evt.preventDefault();
        return false;
    });

    function register(profile) {
        try {
            if (checkFields(profile, ['email', 'password', 'name', 'age', 'photoUrl', 'city', 'contacts'])) {
                $.ajax({
                    type:"POST",
                    url:'/con',
                    data:{method:'register', obj:profile},
                    success:function (res) {
                        if (res.status === 'ok' && res.uid) {
                            //Cookie set and profile ready
                            printSuccess('You have registered successfully! Redirecting to your brand new profile');
                            setTimeout(function () {
                                location.href = '/profile/' + res.uid
                            }, 3000);
                        } else {
                            printError(res.message);
                        }
                    },
                    error:function (e) {
                        printError(e.statusText);
                    },
                    dataType:'json'
                });
            }
        }
        catch (e) {
            printError(e);
        }
    }

    function login(profile) {
        try {
            if (checkFields(profile, ['email', 'password'])) {
                $.ajax({
                    type:"POST",
                    url:'/con',
                    data:{method:'login', obj:profile},
                    success:function (res) {
                        if (res.status === 'ok' && res.uid) {
                            //Cookie set and profile ready
                            printSuccess('You have logged in successfully!');
                            setTimeout(function () {
                                location.href = '/profile/' + res.uid
                            }, 3000);
                        } else {
                            printError(res.message);
                        }
                    },
                    error:function (e) {
                        printError(e.statusText);
                    },
                    dataType:'json'
                });
            }
        }
        catch (e) {
            printError(e);
        }
    }

    function update(profile) {
        try {
            $.ajax({
                type:"POST",
                url:'/con',
                data:{method:'login', obj:profile},
                success:function (res) {
                    if (res.status === 'ok' && res.uid) {
                        //Cookie set and profile ready
                        printSuccess('You have updated profile successfully!');
                        setTimeout(function () {
                            location.href = '/profile/' + res.uid
                        }, 3000);
                    } else {
                        printError(res.message);
                    }
                },
                error:function (e) {
                    printError(e.statusText);
                },
                dataType:'json'
            });

        }
        catch (e) {
            printError(e);
        }
    }

    function checkFields(obj, fieldsArray) {
        for (var i = 0; i < fieldsArray.length; i++) {
            var field = fieldsArray[i];
            if (typeof field === "string") {
                if (!obj.hasOwnProperty(field) || obj[field] === undefined || obj[field] === '' || obj[field] === null) {
                    throw new Error('Missing "' + field + '" field');
                }
            }
            else if (field.hasOwnProperty('regex') && field.hasOwnProperty('name')) {
                if (!obj.hasOwnProperty(field.name) || obj[field.name] === undefined) {
                    throw new Error('Missing "' + field.name + '" field');
                }
                if (!field.regex.match(obj[field.name])) {
                    throw new Error('Field "' + field.name + '" is in invalid formatted');
                }
            }
        }
        return true;
    }

    function printError(msg) {
        $('#alerts').html('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error! </strong>' + msg + '</div>');
    }

    function printSuccess(msg) {
        $('#alerts').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Success! </strong>' + msg + '</div>');
    }

});