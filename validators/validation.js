exports.checkFields=function (obj, fieldsArray) {
    for (var i = 0; i < fieldsArray.length; i++) {
        var field = fieldsArray[i];
        if (typeof field === "string") {
            if (!obj.hasOwnProperty(field) || obj[field] === undefined || obj[field] === '' || obj[field] === null) {
                throw new Error('argument missing: ' +  field);
            }
        }
        else if (field.hasOwnProperty('regex') && field.hasOwnProperty('name')) {
            if (!obj.hasOwnProperty(field.name) || obj[field.name] === undefined) {
                throw new Error('argument missing: ' + field.name);
            }
            if (typeof field.regex==='function')
            {
                if (!field.regex(obj[field.name])){
                    throw new Error('bad argument format: ' + field.name);
                }
            }
            else if (!field.regex.test(obj[field.name])) {
                throw new Error('bad argument format: ' + field.name);
            }
        }
    }
    return true;
}

exports.validateEmail=function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}