var ObjectID = require('mongodb').ObjectID;
var crypto = require('crypto');

module.exports = function (db, hub, options) {
    options = options || {};
    var ttl = options.ttl || 120;
    //Ensure collection index


    return function (req, res, next) {
        //Insert mongo
        var query = {};
        query.lastseen = new Date();
        if (res.locals.currentUser) {
            var user = res.locals.currentUser;
            query._id = user._id.toString();
            query.type = 'builtin';
            query.name = user.name;
            query.photo = user.photo;
            query.geo = user.geo;
            query.city = user.city;
            query.age = user.age;
            query.sex = user.sex;

        } else if (req.session.profile && req.session.profile.email) {
            var user = req.session.profile;
            query._id = 'guest'+crypto.createHash('md5').update(user.email.toLowerCase().trim()).digest("hex");
            query.type = 'guest';
            query.name = user.first_name + ' ' + user.last_name;
            query.photo = user.photo_big || user.photo;
            query.link = user.profile;
            query.network = user.network;
        }

        if (res.locals.currentUser || (req.session.profile && req.session.profile.email)) {
            req.db.collection('users-online', function (err, collection) {
                if (!err) {
                    //Save item with upsert
                    collection.save(query, {upsert:true,safe:false});
                }
            });
        }
        next();
    }
};
