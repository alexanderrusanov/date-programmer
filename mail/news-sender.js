var config = require('../config/config');
var crypto = require('crypto');
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var sender = require('./sender');
var _ = require('underscore');
var async = require('async');

MongoClient.connect(config.mongo.dburi, {db:{}, server:{auto_reconnect:true, poolSize:1}}, function (err, db) {
    if (err) {
        console.log('error connecting to mongo');
    } else {
        dbReady(db);
    }
});

function dbReady(db) {
    db.collection('profiles', function (err, profileCollection) {
        if (!err) {
            profileCollection
                .find({$or:[
                    {_validated:true, _removed:null},
                    {type:"guest"}
                ]}, {name:true, email:true, photo:true, type:true, profile:true, network:true, city:true, geo:true, hash:true})
                .sort({last_seen:-1})
                .toArray(function (err, profiles) {
                    if (!err) {
                        profiles = _.uniq(profiles, function (item) {
                            return item.email;
                        });
                        console.log("sending to " + profiles.length + " addresses");
                        async.forEachLimit(profiles, 20, sendEmail, function (err) {
                            // if any of the saves produced an error, err would equal that error
                            if (!err) {
                                console.log('all done');
                                process.nextTick(function(){
                                    process.exit(0);
                                });

                            } else {
                                console.log('got error:'+err);
                                process.exit(-1);
                            }
                        });
                    }
                });
        }
    });
}

function sendEmail(user, callback) {
    try {
        sender.sendMail({email:user.email, name:user.name}, 'Новости Love++', 'news', user, function (err) {
            console.log("sent to:"+user.email);
            callback();
        });
    } catch (err) {
        console.log('error sending:'+err);
        callback();
    }
}