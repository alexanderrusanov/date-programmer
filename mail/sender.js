var config = require('../config/config');
var jade = require('jade');
var fs = require('fs');
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport(config.mail.smtp);
var htmlToText = require('nodemailer-html-to-text').htmlToText;

transporter.use('compile', htmlToText());

exports.sendMail = function (to, subject, templateName, templateData, callback) {
	//Build view
	try {
		fs.readFile(__dirname + '/templates/' + templateName + '.jade', 'utf8', function (err, data) {
			try {
				var fn = jade.compile(data, {filename: __dirname + '/templates/' + templateName + '.jade'});
				templateData.subject = subject;
				templateData.to = to;
				var html = fn(templateData);

				var mailData = {
					from: config.mail.from,
					to: to,
					subject: subject,
					html: html
				};
				if (templateData.replyTo) {
					mailData.replyTo = templateData.replyTo
				}
				transporter.sendMail(mailData, callback);
			}
			catch (err) {
				callback(err);
			}
		});
	}
	catch
		(err) {
		callback(err);
	}
};

//TEST only
/*
exports.sendMail('alexander.rusanov@gmail.com', 'Test smtp', 'news', {}, function (err) {
	"use strict";
	console.error(err);
});*/
