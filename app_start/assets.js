module.exports = function(assets) {
    var path = require('path');
    assets.root = path.join(__dirname,'../');

    var basePath = "/public";
    assets.addCss(basePath + "/bootstrap/css/bootstrap.css");
    assets.addCss(basePath + "/bootstrap/css/bootstrap-modal.css");
    assets.addCss(basePath + "/pnotify/jquery.pnotify.default.css");
    assets.addCss(basePath + "/terminal/terminal.css");
    assets.addCss(basePath + "/fancybox/jquery.fancybox.css");
    assets.addCss(basePath + "/stylesheets/style.css");

    assets.addJs(basePath + "/javascripts/underscore.js");
    assets.addJs(basePath + "/bootstrap/js/bootstrap.js");
    assets.addJs(basePath + "/bootstrap/js/bootstrap-modalmanager.js");
    assets.addJs(basePath + "/bootstrap/js/bootstrap-modal.js");
    assets.addJs(basePath + "/javascripts/jade.runtime.min.js");
    assets.addJs(basePath + "/fancybox/jquery.fancybox.pack.js");
    assets.addJs(basePath + "/javascripts/script.js");

    assets.addJs(basePath + "/javascripts/moment.js");
    assets.addJs(basePath + "/javascripts/lang/ru.js");
    assets.addJs(basePath + "/javascripts/jquery.json-2.4.min.js");
    assets.addJs(basePath + "/pnotify/jquery.pnotify.min.js");
    assets.addJs(basePath + "/chat/chat_client.js");
    assets.addJs(basePath + "/chat/dialogs.js");
    assets.addJs(basePath + "/chat/common-chat.js");
    assets.addJs(basePath + "/chat/message_notifications.js");

    assets.addJs(basePath + "/terminal/jquery.mousewheel.js","terminal");
    assets.addJs(basePath + "/terminal/terminal.js","terminal");
    assets.addJs(basePath + "/terminal/commander.js","terminal");
    assets.addJs(basePath + "/terminal/sprintf.js","terminal");
    assets.addJs(basePath + "/terminal/term_commands.js","terminal");

    assets.addJs(basePath + "/javascripts/geo-min.js",'catalog');
    assets.addJs(basePath + "/javascripts/jquery.history.js",'catalog');
    assets.addJs(basePath + "/javascripts/search.js",'catalog');
    assets.addJs(basePath + "/javascripts/client_page.js",'catalog');
}