var redis = require('redis');
var url = require('url');
var config = require('../config/config');

exports.createClient = function (){
    var redisURL = url.parse(config.redis.url);
    var client = redis.createClient(redisURL.port, redisURL.hostname, {no_ready_check: true});
    var dbAuth = function() { client.auth(redisURL.auth.split(":")[1]); }
    client.addListener('connected', dbAuth);//For reconection
    client.addListener('reconnected', dbAuth);
    dbAuth();
    return client;
}