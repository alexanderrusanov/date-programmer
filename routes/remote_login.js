var ObjectID = require('mongodb').ObjectID;
var assert = require('assert');
var validation = require('../validators/validation');
var sender = require('../mail/sender');
var crypto = require('crypto');
var config = require('../config/config');

exports.login = function (req, res) {
    if (req.body.token) {
        //Got auth token. Get the profile
        var request = require('request');
        request('http://ulogin.ru/token.php?token=' + req.body.token, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                try {
                    var profile = JSON.parse(body);
                    if (!profile.error) {
                        req.session.profile = profile;
                        res.locals.profile = profile;
                        //Save to collection
                        req.db.collection('profiles', function (err, collection) {
                            if (!err) {
                                //Try find by email first
                                collection.findOne({email:profile.email, _validated:true},{name:true, photo:true, type:true, profile:true, network:true, city:true, geo:true, hash:true}, function (err, item) {
                                    if (item != null) {
                                        //Ok. Login
                                        req.session.user = item._id;
                                        req.session.authorized = true;
                                        res.json(item);
                                    }
                                    else {
                                        profile._id = 'guest' + crypto.createHash('md5').update(profile.email.toLowerCase().trim()).digest("hex");
                                        profile.type = 'guest';
                                        profile.name = profile.first_name + ' ' + profile.last_name;
                                        profile.photo = profile.photo_big || profile.photo;
                                        collection.save(profile, {upsert:true, safe:false});
                                        res.json(profile);
                                    }
                                });
                            }
                        });
                    } else {
                        res.send(403);
                    }
                } catch (err) {
                    res.send(403);
                }
            } else {
                res.send(403);
            }
        })
    } else {
        res.send(403);
    }
}