var ObjectID = require('mongodb').ObjectID;
var assert = require('assert');
var validation = require('../validators/validation');
var sender = require('../mail/sender');
var crypto = require('crypto');
var config = require('../config/config');

function checkFields(req) {
//Check all strong type fields
    if (req) {
        if (req.body.obj.geo && req.body.obj.geo.length == 2) {
            try {
                req.body.obj.geo = [req.body.obj.geo[0] * 1, req.body.obj.geo[1] * 1];
            } catch (err) {
                delete req.body.obj.geo;
            }
        }
        if (req.body.obj.age) {
            try {
                req.body.obj.age = parseInt(req.body.obj.age);
            } catch (err) {
            }
        }
    }
}

function onEdit(req, res) {
    if (req.session.user && req.session.user !== '') {
        delete req.body.obj.email;
        //check bots update
        if (res.locals.currentUser.updated) {
            if ((Date.now() - res.locals.currentUser.updated) < 18000000/*300 minutes*/) {
                delete req.body.obj.updated; //don't update
            }
        }
        //Connect to mongo here and try to update user\
        req.db.collection('profiles', function (err, collection) {
            try {
                collection.update({_id:new ObjectID(req.session.user)}, {$set:req.body.obj}, {safe:true},
                    function (err) {
                        if (err) {
                            res.json({status:'fail', message:'failed to update'});
                        }
                        else {
                            res.json({status:'ok', uid:req.session.user, message:'Profile updated', redir:'/profile/' + req.session.user});
                        }
                    });
            }
            catch (err) {
                res.json({status:'fail', message:'failed to update'});
            }
        });
    }
    else {
        res.json({status:'fail', message:'not logged in'});
    }
}

exports.console = function (req, res) {
    if (req.body) {
        if (req.body.obj) {
            //Clean fields
            delete req.body.obj._id;
            delete req.body.obj._validated;
            try {
                for (var field in req.body.obj) {
                    if (req.body.obj.hasOwnProperty(field)) {
                        var value = req.body.obj[field];
                        if (value && typeof value === 'string') {
                            if (value.length > 160) {
                                req.body.obj[field] = value.substring(0, 160) + '...';
                            }
                        }
                    }
                }
            } catch (err) {
                res.json({status:'fail', message:'bad request'});
                return;
            }
            req.body.obj.updated = Date.now();//Set updated date for all who has activity
            checkFields(req);
        }

        if (req.body.method === "register" && req.body.obj) {
            try {
                validation.checkFields(req.body.obj, [
                    {name:'email', regex:validation.validateEmail},
                    'password',
                    'name',
                    {name:'age', regex:/\d+/},
                    'city'
                ]);
            }
            catch (err) {
                res.json({status:'fail', message:err.message});
                return;
            }
            if (req.session.user && req.session.user !== '' && res.locals.currentUser) {
                res.json({status:'fail', message:'you are logged in already'});
                return;
            }
            //Open regs DB

            //Connect to mongo here and try to find user
            req.db.collection('profiles', function (err, collection) {
                //Check for user
                collection.findOne({email:req.body.obj.email, type:{$ne:'guest'}}, function (err, item) {
                    if (item == null) {
                        //Ok. register
                        var user = req.body.obj;
                        user._validated = false;
                        user._validationKey = require('node-uuid').v4();
                        user.hash = crypto.createHash('md5').update(user.email.toLowerCase().trim()).digest("hex");
                        var validationLink = require('url').resolve('http://' + req.headers.host, '/verify/' + user._validationKey);
                        req.db.collection('registrations-times', function (err, registrationCollection) {
                            registrationCollection.findOne({ip:req.ip}, function (err, item) {
                                if (item && item.date) {
                                    if ((Date.now() - item.date) < 1800000/*30 minutes*/ && item.count > 10) {
                                        res.json({status:'fail', message:'There was to many registration attempts from you IP for less than 30 minutes. Wait please'});
                                        return;
                                    }
                                }
                                registrationCollection.update({ip:req.ip}, {$set:{ip:req.ip, date:new Date()}, $inc:{ count:1 }}, {safe:true, upsert:true}, function (err) {
                                });
                                collection.insert(user, function (err, items) {
                                    req.session.user = items[0]._id;
                                    req.session.authorized = true;
                                    //Send validation email
                                    user.validationLink = validationLink;
                                    sender.sendMail({email:user.email, name:user.name}, 'Подтверждение регистрации Love++', 'verify-user', user, function (err) {
                                        if (err) {
                                            res.json({status:'fail', message:'Failed to send registration:' + err});
                                        }
                                        else {
                                            res.json({status:'ok', uid:req.session.user, message:'Success. Please check your E-mail.', redir:'/profile/' + req.session.user});
                                        }
                                    });


                                });
                            })
                        });
                    }
                    else {
                        res.json({status:'fail', message:'user exists'});
                    }
                });
            })

        }
        else if (req.body.method === "login" && req.body.obj) {
            try {
                validation.checkFields(req.body.obj, [
                    {name:'email', regex:validation.validateEmail},
                    'password'
                ]);
            }
            catch (err) {
                res.json({status:'fail', message:err.message});
                return;
            }
            //Connect to mongo here and try to find user
            req.db.collection('profiles', function (err, collection) {
                //Check for user
                collection.findOne({email:req.body.obj.email, password:req.body.obj.password, _removed:null}, function (err, item) {
                    if (item != null) {
                        //Ok. Login
                        req.session.user = item._id;
                        req.session.authorized = true;
                        res.json({status:'ok', uid:req.session.user, redir:'/profile/' + req.session.user});
                    }
                    else {
                        res.json({status:'fail', message:'bad login/password'});
                    }
                });
            })
        }
        else if (req.body.method === "edit" && req.body.obj) {
            onEdit(req, res);
        }
        else if (req.body.method === "rm" && req.body.obj) {
            onRemove(req, res);
        }
        else if (req.body.method === "passwd" && req.body.obj) {
            onRestorePassword(req, res);
        }
        else {
            res.json({status:'fail', message:'unknown method'});
        }
    }
};

function onRemove(req, res) {
    if (req.session.user && req.session.user !== '' && res.locals.currentUser && res.locals.currentUser._id) {
        //Connect to mongo here and try to update user\
        req.db.collection('profiles', function (err, collection) {
            try {
                var _removeKey = require('node-uuid').v4();
                var validationLink = require('url').resolve('http://' + req.headers.host, '/remove/' + _removeKey);
                collection.findAndModify({_id:res.locals.currentUser._id,password:req.body.obj.password},{},{$set:{_removeKey:_removeKey}},{new:true, safe:true},
                    function (err,item) {
                        if (err || !item) {
                            res.json({status:'fail', message:'failed to remove'});
                        }
                        else {
                            var user = item;
                            user.validationLink = validationLink;
                            sender.sendMail({email:user.email, name:user.name}, 'Удаление профиля на Love++', 'remove-user', user, function (err) {
                                if (err) {
                                    res.json({status:'fail', message:'Failed to send removal email:' + err});
                                }
                                else {
                                    res.json({status:'ok', message:'Success. Check your email for profile deletion link.', redir:'/'});
                                }
                            });
                        }
                    });
            }
            catch (err) {
                res.json({status:'fail', message:'failed to remove'});
            }
        });
    }
    else {
        res.json({status:'fail', message:'not logged in'});
    }
}

function onRestorePassword(req, res) {
    try {
        validation.checkFields(req.body.obj, [
            {name:'email', regex:validation.validateEmail}
        ]);
        if (req.body.obj.key) {
            validation.checkFields(req.body.obj, [
                'password', 'key'
            ]);
        }
    }
    catch (err) {
        res.json({status:'fail', message:err.message});
        return;
    }
    req.db.collection('profiles', function (err, collection) {
        try {
            if (req.body.obj.key && req.body.obj.password) {
                //Check for user
                collection.findOne({email:req.body.obj.email, _restoreKey:req.body.obj.key}, function (err, item) {
                    if (item != null) {
                        //Ok. Send mail
                        var user = item;
                        collection.update({_id:user._id}, {$set:{password:req.body.obj.password}, $unset:{_restoreKey:null}}, {safe:true},
                            function (err) {
                                if (err) {
                                    res.json({status:'fail', message:'failed to reset password'});
                                } else {
                                    //Ok. Login
                                    req.session.user = item._id;
                                    req.session.authorized = true;
                                    res.json({status:'ok', message:'Success. New password set.', uid:req.session.user, redir:'/profile/' + req.session.user});
                                }
                            });
                    }
                    else {
                        res.json({status:'fail', message:'user not found or key is not valid'});
                    }
                });

            } else {
                //Check for user
                collection.findOne({email:req.body.obj.email, _removed:null}, function (err, item) {
                    if (item != null) {
                        //Ok. Send mail
                        var user = item;
                        var _restoreKey = user._restoreKey || require('../utils/utils').uid(5);
                        var validationLink = require('url').resolve('http://' + req.headers.host, '/passwd/' + _restoreKey);
                        collection.update({_id:user._id}, {$set:{_restoreKey:_restoreKey}}, {safe:true},
                            function (err) {
                                if (err) {
                                    res.json({status:'fail', message:'failed to restore password'});
                                } else {
                                    user.validationLink = validationLink;
                                    user.passwordKey = _restoreKey;
                                    sender.sendMail({email:user.email, name:user.name}, 'Восстановление пароля на Love++', 'restore-password', user, function (err) {
                                        if (err) {
                                            res.json({status:'fail', message:'Failed to send restore password email:' + err});
                                        }
                                        else {
                                            res.json({status:'ok', message:'Success. Check your email for restore password key.', redir:'/'});
                                        }
                                    });
                                }
                            });
                    }
                    else {
                        res.json({status:'fail', message:'user not found'});
                    }
                });
            }
        }
        catch (err) {
            res.json({status:'fail', message:'failed to restore password'});
        }
    });
}

